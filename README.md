# powershell-scripts

## keep-screen-alive.ps1
### Description: Keeps the desktop session active by toggling a keyboard key every 4 minutes.
- Default key is the {SCROLLLOCK} key
- can change the key used by the script with the codes listed [here](https://ss64.com/vb/sendkeys.html)
- eg. instaed of "{SCROLLLOCK}" use "{~}"
- to use: 
    1. copy "keep-screen-alive.ps1" to some location
    2. open "Windows Powershell" and navigate to copied script directory
    3. execute the script from the same directory via powershell with "./keep-screen-alive.ps1" (should have tab completion)
