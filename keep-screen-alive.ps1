Clear-Host
Write-Output "Keep-alive with Scroll lock..."

$WShell = New-Object -com "Wscript.shell"

while ($true) {
	$WShell.sendkeys("{SCROLLLOCK}")
	Start-Sleep -Milliseconds 100
	$WShell.sendkeys("{SCROLLLOCK}")
	Start-Sleep -Seconds 240	# 60 * <minutes>
}
